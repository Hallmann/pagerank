import PageRank
from Website import Website

############ 1 ############
# a = Website('A')
# b = Website('B')
# a.links = [b]
# b.links = [a]

############ 2 ############
# a = Website('A')
# b = Website('B')
# c = Website('C')
# a.links = [b]
# b.links = [c]
# c.links = [a]

############ 3 ############
a = Website('A')
b = Website('B')
c = Website('C')
a.links = [b, c]
b.links = [a, c]

############ 4 ############
# a = Website('A')
# b = Website('B')
# c = Website('C')
# a.links = [b]
# b.links = [c]

PageRank.iterative_ranking(dampening_factor=0.85, max_iterations=100, initial_value=1, target_decimal_precision=5)
PageRank.show(decimal_precision=5)
exit(0)
