from typing import Set

import graphviz
from graphviz import Digraph

from Website import Website

websites: Set[Website] = set()


def register_website(website: Website):
    new_name = website.name
    if new_name in [website.name for website in websites]:
        raise Exception(f'Website "{new_name}" already registered')
    websites.add(website)


def iterative_ranking(dampening_factor: float = 0.85,
                      max_iterations: int = 20,
                      initial_value: float = 1,
                      target_decimal_precision: int = None) -> None:
    if not (0 < dampening_factor < 1 and 0 < max_iterations <= 1000):
        raise ValueError()

    stop_on_precision_reached = False
    if type(target_decimal_precision) == int:
        stop_on_precision_reached = True
        target_decimal_precision += 2

    for website in websites:
        website.page_rank = initial_value

    for i in range(max_iterations):
        for website in websites:
            website.prev_page_rank = website.page_rank
            website.page_rank = 1 - dampening_factor

        for website in websites:
            links = website.links
            for link in links:
                link.page_rank += dampening_factor * (website.prev_page_rank / len(links))

        if stop_on_precision_reached:
            max_difference = max(abs(website.page_rank - website.prev_page_rank) for website in websites)
            target_value = 0.1 ** target_decimal_precision
            if max_difference < target_value:
                print(f'target precision reached after {i} iterations')
                print(f'Required:       {target_value:.{target_decimal_precision + 1}f}')
                print(f'Max difference: {max_difference:.{target_decimal_precision + 1}f}')
                return
    if stop_on_precision_reached:
        max_difference = max(abs(website.page_rank - website.prev_page_rank) for website in websites)
        target_value = 0.1 ** target_decimal_precision
        print('Target precision not reached! Consider increasing iterations.')
        print(f'Required:       {target_value:.{target_decimal_precision + 1}f}')
        print(f'Max difference: {max_difference:.{target_decimal_precision + 1}f}')


def show(decimal_precision: int = 2) -> None:
    def name_and_rank(website: Website) -> str:
        return f'{website.name} {website.page_rank:.{decimal_precision}}'

    graph = Digraph()
    for website in websites:
        for link in website.links:
            graph.edge(name_and_rank(website), name_and_rank(link))

    try:
        graph.render('generated graph', view=True, cleanup=True, format='svg')
    except graphviz.ExecutableNotFound:
        print()
        print('Error: graphviz not installed. On Linux run the following command:')
        print('sudo apt install graphviz -y')
