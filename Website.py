from typing import List

import PageRank


class Website:
    def __init__(self, name: str):
        self.name: str = name
        self.links: List[Website] = []
        self.page_rank: float = None
        self.prev_page_rank: float = None
        PageRank.register_website(self)
