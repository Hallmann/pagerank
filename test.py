from unittest import TestCase

import PageRank
from Website import Website


class IntegrationTest(TestCase):
    def tearDown(self):
        PageRank.websites.clear()

    def create_circle(self, size: int):
        self.assertTrue(1 <= size <= 26)
        websites = []
        for i in range(size):
            name = chr(ord('A') + i)
            websites.append(Website(name))
        for i in range(size):
            link_index = (i + 1) % size
            websites[i].links = [websites[link_index]]

    def test_create_website(self):
        w = Website('test')
        self.assertIn(w, PageRank.websites)

    def test_duplicate_website(self):
        Website('duplicate')
        with self.assertRaises(Exception) as context:
            Website('duplicate')
        self.assertIn('Website "duplicate" already registered', str(context.exception))
        self.assertEqual(1, len(PageRank.websites))

    def test_page_rank_default(self):
        self.create_circle(10)

        PageRank.iterative_ranking()

        for ws in PageRank.websites:
            self.assertEqual(1, ws.page_rank)

    def test_page_rank_init_0(self):
        self.create_circle(10)
        required_precision = 5

        PageRank.iterative_ranking(max_iterations=100, initial_value=0, target_decimal_precision=required_precision)

        for ws in PageRank.websites:
            self.assertAlmostEqual(1, ws.page_rank, required_precision)
